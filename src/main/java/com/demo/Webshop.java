package com.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Webshop {

	public static void main(String[] args) {
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://demowebshop.tricentis.com/");
		
		driver.manage().window().maximize();
		
		WebElement register = driver.findElement(By.xpath("//div[@class='header-links']//a[contains(text(),'Register')]"));
		register.click();
		
		driver.findElement(By.id("gender-male")).click();
		
		driver.findElement(By.id("FirstName")).sendKeys("manu");
		
		driver.findElement(By.id("LastName")).sendKeys("Warrier");
		
		driver.findElement(By.id("Email")).sendKeys("manuwarrier@gmail.com");
		
		driver.findElement(By.id("Password")).sendKeys("Manu@123");
		
		driver.findElement(By.id("ConfirmPassword")).sendKeys("Manu@123");
		
		driver.findElement(By.name("register-button")).click();

	}
}
