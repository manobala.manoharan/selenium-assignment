package com.demo;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class WebShopComputers {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://demowebshop.tricentis.com/");
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
		WebElement login = driver.findElement(By.xpath("//a[@class='ico-login']"));
		login.click();
		
		WebElement email = driver.findElement(By.id("Email"));
		email.sendKeys("manuwarrier@gmail.com");
		
		WebElement password = driver.findElement(By.id("Password"));
		password.sendKeys("Manu@123");
		
		WebElement clicklogin = driver.findElement(By.xpath("//input[@value='Log in']"));
		clicklogin.click();
		
		WebElement product = driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Computers')]"));
		product.click();
		
		WebElement desktop = driver.findElement(By.xpath("//img[@alt='Picture for category Desktops']"));
		desktop.click();
		
		WebElement sortby = driver.findElement(By.id("products-orderby"));
		Select select = new Select(sortby);
		select.selectByVisibleText("Price: Low to High");
		
		WebElement cheapcomputer = driver.findElement(By.xpath("//h2[@class='product-title']//a[contains(text(),'Build your own cheap computer')]"));
		cheapcomputer.click();
				
		WebElement addtocart = driver.findElement(By.id("add-to-cart-button-72"));
		addtocart.click();
		
		WebElement shippingcart = driver.findElement(By.xpath("//span[text()='Shopping cart']"));
		shippingcart.click();
		
		WebElement termsandcondition = driver.findElement(By.id("termsofservice"));
		termsandcondition.click();
		
		WebElement checkout = driver.findElement(By.id("checkout"));
		checkout.click();
		
		WebElement billingaddress = driver.findElement(By.xpath("//input[@title='Continue']"));
		billingaddress.click();
		
		WebElement shippingaddress = driver.findElement(By.xpath("(//input[@title='Continue'])[2]"));
		shippingaddress.click();
		
		WebElement shippingmethod = driver.findElement(By.xpath("//input[@onclick='ShippingMethod.save()']"));
		shippingmethod.click();
		
		WebElement paymentmethod = driver.findElement(By.xpath("//input[@onclick='PaymentMethod.save()']"));
		paymentmethod.click();
		
		WebElement paymentinformation = driver.findElement(By.xpath("//input[@onclick='PaymentInfo.save()']"));
		paymentinformation.click();
		
		WebElement confirmorder = driver.findElement(By.xpath("//input[@onclick='ConfirmOrder.save()']"));
		confirmorder.click();
		
		WebElement successfullorder = driver.findElement(By.xpath("//div[@class='title']"));
		String printorderresult = successfullorder.getText();
		System.out.println(printorderresult);
		
		WebElement orderno = driver.findElement(By.xpath("//ul[@class='details']"));
		String ordernumber = orderno.getText();
		System.out.println(ordernumber);
		
		WebElement thankyoustatus = driver.findElement(By.xpath("//input[@value='Continue']"));
		thankyoustatus.click();
		
		WebElement logout = driver.findElement(By.xpath("//a[@class='ico-logout']"));
		logout.click();
		
		driver.quit();
		
	}
}
