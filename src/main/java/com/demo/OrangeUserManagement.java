package com.demo;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class OrangeUserManagement {

	public static void main(String[] args) throws AWTException, InterruptedException {
		
			WebDriver driver = new ChromeDriver();
			
			driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
			
			driver.manage().window().maximize();
			
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			
			WebElement username = driver.findElement(By.name("username"));
			username.sendKeys("Admin");
			
			WebElement password = driver.findElement(By.name("password"));
			password.sendKeys("admin123");
			
			WebElement login = driver.findElement(By.xpath("//button[text()=' Login ']"));
			login.click();
			
			WebElement admin = driver.findElement(By.xpath("//span[text()='Admin']"));
			admin.click();
			
			WebElement add = driver.findElement(By.xpath("//button[@class='oxd-button oxd-button--medium oxd-button--secondary']"));
			add.click();
			
			WebElement userrole = driver.findElement(By.xpath("//i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow']"));
			userrole.click();
			
			Robot robot = new Robot();
			
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);
			
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);
			
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			
			userrole.click();
			
			WebElement employeename = driver.findElement(By.xpath("//input[@placeholder='Type for hints...']"));
			employeename.sendKeys("Jo");
			
			Thread.sleep(5000);
			
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);
			
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);

			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			
			WebElement status = driver.findElement(By.xpath("(//i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow'])[2]"));
			status.click();
			
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);
			
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);

			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			
			status.click();
			
			WebElement user = driver.findElement(By.xpath("(//input[@class='oxd-input oxd-input--active'])[2]"));
			user.sendKeys("Jonty Rhodes");
			
			WebElement pass = driver.findElement(By.xpath("//input[@type='password']"));
			pass.sendKeys("Jonty@123");
			
			WebElement confirmpass = driver.findElement(By.xpath("(//input[@type='password'])[2]"));
			confirmpass.sendKeys("Jonty@123");
			
			Thread.sleep(5000);
			
			WebElement savebutton = driver.findElement(By.xpath("//button[@type='submit']"));
			savebutton.click();
			
			driver.quit();
			
	}
}
