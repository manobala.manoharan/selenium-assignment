package com.demo;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TaskNew {

	
	public static void main(String[] args) throws InterruptedException {
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.selenium.dev/selenium/docs/api/java/index.html?overview-summary.html");
		
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	        
	        driver.findElement(By.xpath("//a[text()='Frames']")).click();
	        
	        driver.switchTo().frame("packageListFrame");
	        
	        driver.findElement(By.xpath("//ul[@title='Packages']//a[text()='org.openqa.selenium']")).click();
	       
	        driver.navigate().refresh();
	      
	        Thread.sleep(3000);
	      
	        driver.switchTo().frame("packageFrame");
	        
	        driver.findElement(By.xpath("//a[text()='AbstractAnnotations']")).click();
	        
	        Thread.sleep(3000);
	        
	        driver.quit();
	        
	        
	}
}
